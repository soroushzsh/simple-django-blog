from django.urls import path
from . import views

urlpatterns = [
    path('', views.PostListView.as_view(), name='post_list'),
    path('about/', views.AboutView.as_view(), name='about'),
    path('post/<int:pk>', views.PostDetailView.as_view(), name='post_detail'),
    path('addpost/', views.CreatePostView.as_view(), name='add_new_post'),
    path('post/<int:pk>/edit', views.UpdatePostView.as_view(), name='edit_post'),
    path('post/<int:pk>/delete', views.PostDeleteView.as_view(), name='delete_post'),
    path('post/<int:pk>/delete', views.PostDeleteView.as_view(), name='delete_post'),
    path('drafts/', views.DraftListView.as_view(), name='drafts'),
]
