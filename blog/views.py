from django.contrib.auth.decorators import login_required  # for function based views
from django.utils import timezone
from django.shortcuts import render, get_object_or_404, redirect
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin  # for class based views
from .forms import PostForm, CommentForm
from django.urls import reverse_lazy

from .models import Post, Comment


class AboutView(generic.TemplateView):
    template_name = 'about.html'


class PostListView(generic.ListView):
    model = Post

    # it defines to grab which posts
    def get_queryset(self):
        return Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')


class PostDetailView(generic.DetailView):
    model = Post


class CreatePostView(LoginRequiredMixin, generic.CreateView):
    login_url = '/login/'
    redirect_field_name = 'blog/post_detail.html'

    form_class = PostForm

    model = Post


class UpdatePostView(LoginRequiredMixin, generic.UpdateView):
    login_url = '/login/'
    redirect_field_name = 'blog/post_detail.html'

    form_class = PostForm

    model = Post


class PostDeleteView(LoginRequiredMixin, generic.DetailView):
    model = Post
    success_url = reverse_lazy('post_list')


class DraftListView(LoginRequiredMixin, generic.ListView):
    login_url = '/login/'
    redirect_field_name = 'blog/post_list.html'
    model = PostDeleteView

    def get_queryset(self):
        return Post.objects.filter(published_date__isnull=True).order_by('created_date')


#############################################
#############################################
# handle comments

@login_required
def add_comment_to_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == 'POST':
        form = CommentForm(request.POST)




























